.. 

treebeard problem
=================

Clone this project and access /admin/ with the newly
created administration user. Navigate to folders and
try to add a folder that is child of root named 'a'.
Then add another folder that is a child of root named
'b'. Then add another folder that is a child of root
named 'c'. Adding 'c' should raise an error about path
uniqueness.

Add folder 'A' as a child of 'a'. Add another folder
'AA' as a child of 'a'. Add another folder 'AAA' as
a child of 'a'. ADding 'AAA' should raise an error
about path uniqueness.


Quickstart
----------

To bootstrap the project as a developer::

    virtualenv --distribute ve
    source ve/bin/activate
    pip install -r requirements.txt
    python manage.py syncdb
    python manage.py runserver

