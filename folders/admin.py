from django.contrib import admin
from folders import models
from folders import forms

admin.site.register(models.Folder, forms.FolderFormAdmin)

