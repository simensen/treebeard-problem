from treebeard.admin import TreeAdmin
from treebeard.forms import MoveNodeForm
from folders.models import Folder

class FolderForm(MoveNodeForm):
    class Meta:
        model = Folder
        fields = ('name',)

class FolderFormAdmin(TreeAdmin):
    form = FolderForm

    def queryset(self, request):
        return Folder.objects.all()
