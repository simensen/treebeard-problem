from django.db import models
from treebeard.mp_tree import MP_Node

class FolderManager(models.Manager):
    """
    A Folder Manager
    """
    pass

class Folder(MP_Node):
    """
    A Folder
    """
    node_order_by = ['name']

    name = models.CharField(max_length=128)

    objects = FolderManager()

    def __unicode__(self):
        return self.name
